/**
 * Created by Dmitry Vereykin on 7/24/2015.
 */
public class NegativeUnitsOnHand extends Exception {
    public NegativeUnitsOnHand() {
        super("Error! Entered number for units on hand must be positive.");
    }
}

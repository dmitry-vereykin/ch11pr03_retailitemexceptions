/**
 * Created by Dmitry Vereykin on 7/24/2015.
 */
public class RetailItemDemo {
    public static void main(String[] args) {
        RetailItem item;

        try {
            item = new RetailItem("Jacket", 12, 59.95);
        } catch (NegativePrice negativePrice) {
            System.out.println(negativePrice.getMessage());
        } catch (NegativeUnitsOnHand negativeUnitsOnHand) {
            System.out.println(negativeUnitsOnHand.getMessage());
        }

        try {
            item = new RetailItem("Designer Jeans", -25, 34.95);
        } catch (NegativePrice negativePrice) {
            System.out.println(negativePrice.getMessage());
        } catch (NegativeUnitsOnHand negativeUnitsOnHand) {
            System.out.println(negativeUnitsOnHand.getMessage());
        }

        try {
            item = new RetailItem("Shirt", 20, -24.95);
        } catch (NegativePrice negativePrice) {
            System.out.println(negativePrice.getMessage());
        } catch (NegativeUnitsOnHand negativeUnitsOnHand) {
            System.out.println(negativeUnitsOnHand.getMessage());
        }


    }
}
